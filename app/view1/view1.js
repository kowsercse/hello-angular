'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$http', function ($http) {
  var self = this;
  // self.url = apiConfig.apiBaseUrl + "token";
  self.url = "http://localhost:8000";

  this.postData = function (jsonParams) {
    console.log("Sending data via post request")
    console.log(jsonParams);
    $http.post(self.url, jsonParams).then(function (response) {
      console.log("Success");
      console.log(response);
    }, function () {
      console.log("Failure");
    });
  }

}]);
